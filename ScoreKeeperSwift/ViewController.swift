//
//  ViewController.swift
//  ScoreKeeperSwift
//
//  Created by Zachary Cole on 1/23/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

// TODO keyboard hide/show
// ?? Where is all the UI code? Is there a way to view this?
// Constraints, etc

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var fieldL: UITextField!
    @IBOutlet weak var labelL: UILabel!
    @IBOutlet weak var stepperL: UIStepper!
    @IBOutlet weak var fieldR: UITextField!
    @IBOutlet weak var labelR: UILabel!
    @IBOutlet weak var stepperR: UIStepper!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        fieldL.text = "Enter player 1 here"
        fieldR.text = "Enter player 2 here"
        labelL.text = "0"
        labelR.text = "0"
        
        stepperL.addTarget(self, action: Selector.init(), forControlEvents: UIControlEvents.AllEditingEvents)
        
        self.fieldL.delegate = self
        self.fieldR.delegate = self
    }

    @IBAction func stepLOn(sender: UIStepper) {
        labelL.text = String(format: "%.0f", sender.value)
        self.view.endEditing(true)
    }
    
    @IBAction func stepROn(sender: UIStepper) {
        labelR.text = String(format: "%.0f", sender.value)
        self.view.endEditing(true)
    }
    
    @IBAction func resetAction(sender: AnyObject) {
        fieldL.text = "Enter player 1 here"
        fieldR.text = "Enter player 2 here"
        labelL.text = "0"
        labelR.text = "0"
        stepperL.value = 0;
        stepperR.value = 0;
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}

